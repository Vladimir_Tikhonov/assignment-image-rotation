#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/pixel.h"

#pragma once

FILE * open_file(const char * filename, const char * options);

void close_file(FILE * file);

struct pixelmap create_pixelmap(uint64_t width, uint64_t height);

long get_padding(uint64_t width);
