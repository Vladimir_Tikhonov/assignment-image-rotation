#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include"../include/pixel.h"

#pragma once

enum read_status  {
  READ_OK = 0,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_DATA
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

