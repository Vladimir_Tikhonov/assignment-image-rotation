#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/image.h"
#include "../include/pixel.h"

#pragma once

struct pixelmap img_rot(struct pixelmap pixelmap);

