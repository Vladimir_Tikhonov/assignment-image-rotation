#include <stdint.h>
#include <stdio.h>

#pragma once

/*Структура 24-х разрядного rgb-пикселя*/
struct pixel{

	uint8_t r, g, b;

};

/*Структура пиксельных данных изображения;*/
struct pixelmap {

	uint64_t width, height;
	struct pixel * data; 
	
};
