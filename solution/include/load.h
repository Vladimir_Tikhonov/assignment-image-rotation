#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/image.h"
#include "../include/pixel.h"
#include "../include/validation.h"

#pragma once

enum read_status loadBMP (struct pixelmap *pixelmap, FILE * file);
