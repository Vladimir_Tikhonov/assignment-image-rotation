#include "../include/image.h"


FILE * open_file(const char * filename, const char * options) 
{
    FILE * file;
    file = fopen(filename, options);
    return file;
}

void close_file(FILE * file)
{
    fclose(file);
}

struct pixelmap create_pixelmap(uint64_t width, uint64_t height)
{
    struct pixelmap pix = {width, height, NULL};
    pix.data = malloc(width * height * sizeof(struct pixel));
    return pix;
}

long get_padding(uint64_t width)
{
    long padding = ((long)(4 - (width * sizeof(struct pixel)) % 4) % 4);
    return padding;
}
