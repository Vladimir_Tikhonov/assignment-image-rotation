#include "../include/save.h"

enum write_status saveBMP (struct pixelmap pixelmap, FILE * file) {

	uint16_t tmpUInt16 = 0;
	uint64_t tmpUInt64 = 0;
	int32_t tmpInt32 = 0;

	/*[0-й байт] Отметка формата;*/
	fputc ('B', file);
	fputc ('M', file);

	/*[2-й байт] Размер файла (размер заголовка файла + размер изображения * разрядность пикселя);*/
	tmpUInt64 = 14 + 40 + pixelmap.width * pixelmap.height * 24;
	fwrite (&tmpUInt64, sizeof(uint32_t), 1, file);

	/*[6-й байт] Зарезервировано;*/
	fputc (0, file);
	fputc (0, file);
	fputc (0, file);
	fputc (0, file);

	/*[10-й байт] Положение пиксельных данных относительно начала файла;*/
	tmpUInt64 = 14 + 40;
	fwrite (&tmpUInt64, sizeof(uint32_t), 1, file);

	/*[14-й байт] Размер структуры и по совместительству версия формата;*/
	tmpUInt64 = 40;
	fwrite (&tmpUInt64, sizeof(uint32_t), 1, file);

	/*[18-й байт] Ширина изображения;*/
	tmpInt32 = (int32_t) pixelmap.width;
	fwrite (&tmpInt32, sizeof(int32_t), 1, file);

	/*[22-й байт] Высота изображения;*/
	tmpInt32 = (int32_t) pixelmap.height;
	fwrite (&tmpInt32, sizeof(int32_t), 1, file);

	/*[26-й байт] Значение формата;*/
	tmpUInt16 = 1;
	fwrite (&tmpUInt16, sizeof(uint16_t), 1, file);

	/*[28-й байт] Бит на пиксель;*/
	tmpUInt16 = 24;
	fwrite (&tmpUInt16, sizeof(uint16_t), 1, file);

	/*[30-й байт] Метод компрессии;*/
	tmpUInt64 = 0;
	fwrite (&tmpUInt64, sizeof(uint32_t), 1, file);

	/*[34-й байт] Размер пиксельных данных в байтах;*/
	tmpUInt64 = 0; 
	fwrite (&tmpUInt64, sizeof(uint32_t), 1, file);

	/*[38-й байт] PPM по горизонтали;*/
	tmpInt32 = 3780;
	fwrite (&tmpInt32, sizeof(int32_t), 1, file);

	/*[42-й байт] PPM по вертикали;*/
	tmpInt32 = 3780;
	fwrite (&tmpInt32, sizeof(int32_t), 1, file);

	/*[46-й байт] Размер таблицы цветов в ячейках;*/
	tmpUInt64 = 0;
	fwrite (&tmpUInt64, sizeof(uint32_t), 1, file);

	/*[50-й байт] Количество ячеек от начала таблицы цветов до последней используемой (включая её саму);*/
	tmpUInt64 = 0;
	fwrite (&tmpUInt64, sizeof(uint32_t), 1, file);

	long padding = get_padding(pixelmap.width);

	/*[54-й байт] Пиксельные данные;*/
	struct pixel padding_bytes[] = {{0},{0},{0}};
	for (size_t i = 0; i < pixelmap.height; i++) {
        fwrite(&(pixelmap.data[i * pixelmap.width]), sizeof(struct pixel), pixelmap.width, file); 
        fwrite(padding_bytes, 1, padding, file);
    }

	if (feof(file)){
		return WRITE_ERROR;
	}
	else{
		return WRITE_OK;
	}

}
