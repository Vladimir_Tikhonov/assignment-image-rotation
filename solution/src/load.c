#include "../include/load.h"

enum read_status loadBMP (struct pixelmap *pixelmap, FILE * file){

    uint32_t pixelDataOffset = 0;
	int32_t width = 0, height = 0;
	uint16_t bitsPerPixel = 0;

    /*Получаем смещение пиксельных данных;*/
    fseek (file, 10, SEEK_SET);
	fread (&pixelDataOffset, sizeof(uint32_t), 1, file);

	if (pixelDataOffset ==  0){
		return READ_INVALID_HEADER;
	}

    /*Получаем размеры изображения;*/
    fseek (file, 18, SEEK_SET);	
	fread (&width, sizeof(int32_t), 1, file);
	fread (&height, sizeof(int32_t), 1, file);

	if (width == 0 || height == 0){
		return READ_INVALID_HEADER;
	}

    /*Количество бит на пиксель;*/
	fseek (file, 28, SEEK_SET);
	fread (&bitsPerPixel, sizeof(uint16_t), 1, file);

	if (bitsPerPixel == 0){
		return READ_INVALID_HEADER;
	}

    /*Поддерживаем только 24-х битные изображения;*/
	if (bitsPerPixel != 24) {
		return READ_INVALID_BITS;
	}

	long padding = get_padding(width);

	*pixelmap = create_pixelmap(width, height);

    fseek (file, pixelDataOffset, SEEK_SET);
    
	for (size_t i = 0; i < pixelmap->height; i++) {
            fread(&(pixelmap->data[i * pixelmap->width]), sizeof(struct pixel), pixelmap->width, file);
            fseek(file, padding, SEEK_CUR);
    }

	if (pixelmap->data == NULL){
		return READ_INVALID_DATA;
	}

    return READ_OK;
}
