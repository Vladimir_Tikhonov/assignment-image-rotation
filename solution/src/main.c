#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/image.h"
#include "../include/load.h"
#include "../include/pixel.h"
#include "../include/rotate.h"
#include "../include/save.h"
#include "../include/validation.h"


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3){
        printf("Usage: ./image-transformer <source-image> <transformed-image>\n");
        return 1;
    }

    const char *filename_in = argv[1];
    FILE * file_in = open_file(filename_in, "rb");

    struct pixelmap pixelmap = {0, 0, NULL};
	enum read_status load_valid = loadBMP(&pixelmap, file_in);
    close_file(file_in);

    if(load_valid != READ_OK){
        return (int)(load_valid);
    }

    struct pixelmap pix_rot = img_rot(pixelmap);

    const char *filename_out = argv[2];
    FILE * file_out = open_file(filename_out, "wb");

    enum write_status save_valid = saveBMP(pix_rot, file_out);
    close_file(file_out);

    if (save_valid != WRITE_OK){
        return (int)(save_valid);
    }

    free(pixelmap.data);
    free(pix_rot.data);

    return 0;
}
