#include"../include/rotate.h"

struct pixelmap img_rot(struct pixelmap pixelmap)
{
    struct pixelmap pix_rot = create_pixelmap(pixelmap.height, pixelmap.width);

    for (size_t i = 0; i < pixelmap.height; i++)
    {
        for (size_t j = 0; j < pixelmap.width; j++)
        {
            pix_rot.data[(j * pix_rot.width) + (pix_rot.width - i - 1)] = pixelmap.data[i * pixelmap.width + j];
        }
    }
    return pix_rot;
}
